import pygame
import os

class Map(object):
    """Represents the map of the game

    Control the background of the game and how it is constructed.

    """

    def __init__(self, screen, stage):
        self.screen = screen
        self.stage = stage
        path_to_bg_img = os.path.join('img', 'background.png')
        self.background = pygame.image.load(path_to_bg_img).convert()

    def show(self, ):
        """Place the background in the given screen."""
        background_surface = pygame.Surface((self.stage.width, self.stage.height))
        for y in range(0,self.stage.height,self.background.get_height()):
            for x in range(0,self.stage.width,self.background.get_width()):
                background_surface.blit(self.background, (x,y))

        self.screen.blit(background_surface, self.stage)