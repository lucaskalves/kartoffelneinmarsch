Kartoffelneinmarsch
===================

A tower defense game. Damn potatoes are trying to invade your
land. You must kill them. You must destroy them.

NO POTATO SHALL SURVIVE.

## How to Run ##

In the folder you downloaded the source code, run:

    python kartoffelneinmarsch.py    

## Dependencies ##

* Python 2.7
* Pygame 1.9

## Game Rules ##

1st Rule: The potatoes shall not pass     
2nd Rule: THE POTATOES SHALL NOT PASS!     
3rd Rule: You can destroy potatoes by buying towers. Towers cost points.     
4th Rule: You earn points only if you don't allow any potato to pass the battlefield      
5th Rule: There is no way to restart the game.      
6th RULE: If this is your first time at KARTOFFELNEINMARSCH, you HAVE to win.     

## Authors ##

[Adolfo Schneider](http://twitter.com/adolfosrs)    
[Lucas Kreutz](http://lucaskreutz.com.br)

## License ##

The good and old MIT License. See the license.txt file in the root folder.

## Images Credits ##

The icons were found at [findicons.com](http://findicons.com/). If you are
ofended by its usage tell us and we will remove them.   

Tower Icon: <http://www.hybridworks.jp/>    
Potato Icon: <http://www.dlanham.com/>    
Sweet Potato Icon: <http://www.clker.com/clipart-10795.html>    
Fence Icon : <http://mattahan.deviantart.com/>    
French Fries: <http://sweetclipart.com/>    
Grass background: <http://opengameart.org/forumtopic/2d-art-grass-tile-request>    
Sack background: <http://www.textureshaven.com/fabric-textures/burlap-sack-textures/>      
