import pygame
import os
import json

class Wave(object):

	def __init__(self, enemy_type, number_of_potatoes, speed, award):
		self.enemy_type = enemy_type
		self.number_of_potatoes = number_of_potatoes
		self.speed = speed
		self.award = award