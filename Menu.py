import pygame
import os

class Menu(object):
    """Control of the game menu

    It takes care of the menu of the game, that is, the section in which the
    towers are exposed and the pause button.

    """

    def __init__(self, screen, menu_area, Level, Player):
        self.screen = screen
        self.menu_area = menu_area
        self.level = Level
        self.player = Player

        path_to_tower_img = os.path.join('img', 'menu_item_tower.png')
        path_to_tower_selected_img = os.path.join('img', 'menu_item_selected_tower.png')
        self.tower_icon = pygame.image.load(path_to_tower_img).convert_alpha()
        self.tower_selected_icon = pygame.image.load(path_to_tower_selected_img).convert_alpha()
        self.tower_selected = False

        path_to_background_img = os.path.join('img', 'menu_background.jpg')
        self.background = pygame.image.load(path_to_background_img).convert()

    def show(self):
        """Place the menu in the given screen."""

        font = pygame.font.Font(None, 25) 

        # print the background
        for y in range(self.menu_area.top,self.menu_area.bottom,self.background.get_height()):
            for x in range(self.menu_area.left,self.menu_area.right,self.background.get_width()):
                self.screen.blit(self.background, (x,y))

        # print the tower icon
        self.screen.blit(self.tower_icon, 
            (self.menu_area.right-self.tower_icon.get_width(),self.menu_area.top))

        s = str(self.level.name)
        level_name = font.render(s, 1, (0, 0, 0))
        self.screen.blit(level_name, (325, 505))

        #print player health
        s = "Your Health: " + str(self.player.health)
        text = font.render(s, 1, (0, 0, 0))
        self.screen.blit(text, (5,530))

        s = "Your Points: " + str(self.player.points)
        text = font.render(s, 1, (0, 0, 0))
        self.screen.blit(text, (5,545))

        s = "Wave Number: " + str(self.level.current_wave_number)
        text = font.render(s, 1, (0, 0, 0))
        self.screen.blit(text, (5,560))

        s = "Wave Award: " + str(self.level.waves[self.level.current_wave_number-1].award)
        text = font.render(s, 1, (0, 0, 0))
        self.screen.blit(text, (5,575))

        s = "Enemy Type: " + str(self.level.waves[self.level.current_wave_number-1].enemy_type)
        text = font.render(s, 1, (0, 0, 0))
        self.screen.blit(text, (200,530))

        s = "Tower Price: 10"
        text = font.render(s, 1, (0, 0, 0))
        self.screen.blit(text, (570, 580))

    def click(self):
        if self.tower_selected:
            self.tower_selected = False
            self.screen.blit(self.tower_icon, 
                (self.menu_area.right-self.tower_icon.get_width(),self.menu_area.top))
        else:
            self.tower_selected = True
            self.screen.blit(self.tower_selected_icon, 
                (self.menu_area.right-self.tower_icon.get_width(),self.menu_area.top))

    def tower_is_selected(self):
        return self.tower_selected