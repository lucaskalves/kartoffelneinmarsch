import pygame
import os

class Player(object):
    def __init__(self, health, points, current_level_number):
    	self.health = health
    	self.points = points
    	self.current_level_number = current_level_number

    def increase_health(self, increment):
    	self.health += increment

    def decrease_health(self, decrement):
    	self.health -= decrement

    def increase_points(self, increment):
    	self.points += increment

    def decrease_points(self, decrement):
    	self.points -= decrement
