import pygame
import os
import json
from Wave import Wave

class Level(object):

    def __init__(self, screen, level_path):
        
        json_data=open(level_path)
        data = json.load(json_data)
        json_data.close()

        self.screen=screen
        self.name = data["name"]
        self.number_of_waves = data["number_of_waves"]
        self.current_wave_number = 1

        self.waves = list()
        for wave in data["waves"]:
            number_of_potatoes = wave["number_of_potatoes"]
            speed = wave["speed"]
            award = wave["award"]
            enemy_type = wave["enemy_type"]
            self.waves.append(Wave(enemy_type, number_of_potatoes, speed, award))