import pygame
import os

class Tower(object):
    def __init__(self, screen, stage):
        self.screen = screen
        self.stage = stage
        self.temporary = True
        self.health = 1000
        self.damage = 8    
        self.range = 45 #to customize
        self.price = 10

        path_to_tower_img = os.path.join('img', 'tower.png')
        
        self.tower_img = pygame.image.load(path_to_tower_img).convert_alpha()
        self.tower_area = None
        self.base_area = None
        self.range_area = None

    def show_temporary(self, pos):
        pos = (pos[0] - self.tower_img.get_width()/2, pos[1]- self.tower_img.get_height()/2)
        self.tower_area = pygame.Rect(pos, self.tower_img.get_size())
        self.base_area = pygame.Rect((self.tower_area.x, self.tower_area.y+80),
            (self.tower_area.width, self.tower_area.height-80))
        self.range_area = pygame.Rect((self.tower_area.x-self.range, self.tower_area.y-self.range),
            (self.tower_area.width+2*self.range, self.tower_area.height+2*self.range))

        if self.is_in_stage():
            self.screen.blit(self.tower_img, self.tower_area)

    def fix_tower(self): 
        self.temporary = False

    def is_in_stage(self):
        if not self.tower_area is None:
            return self.stage.contains(self.base_area)
        else:
            return False

    def show(self):
        font = pygame.font.Font(None, 30)
        s = str(self.health)
        text = font.render(s, 1, (250, 250, 250))
        self.screen.blit(self.tower_img, self.tower_area)
        self.screen.blit(text, (self.tower_area.x,self.tower_area.y))

    def get_collision_area(self):
        return self.base_area

    def get_range_area(self):
        return self.range_area

    def colides_with(self, itens_list):
        return self.base_area.collidelist(itens_list) >= 0

    def is_in_range(self, itens_list):
        return self.range_area.collidelist(itens_list) >= 0

    def attack(self,potato):
        potato.health -= self.damage