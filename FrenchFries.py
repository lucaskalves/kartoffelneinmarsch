import pygame
import os
import random
from Map import Map
from Enemy import Enemy
from Potato import Potato

class FrenchFries(Potato):
    """Represents the damn potato, the worst Boesewicht of the world."""

    def __init__(self, screen, stage, fps, speed=30, pos_x = -50, pos_y = -1):
        super(FrenchFries, self).__init__(screen, stage, fps, speed, pos_x = -50, pos_y = -1)
        self.health = 800
        self.damage = 10
        self.surface = pygame.image.load(os.path.join('img', 'french_fries.png')).convert_alpha()
        potato_width = self.surface.get_width()
        potato_height = self.surface.get_height()
        
        x = pos_x
        y = random.randint(stage.top, stage.bottom - potato_height) if pos_y < 0 else pos_y
        self.character = pygame.Rect(x, y, potato_width, potato_height)
        self.health_display = pygame.Rect(x, y, 50, 100)

        self.collision_area = pygame.Rect((x, y+22), (potato_width, potato_height-22))

    def show(self):
        """Show the potato in the screen."""
        if(self.character.right > 0 and self.character.left < self.stage.width):
            font = pygame.font.Font(None, 20)
            s = str(self.health)
            text = font.render(s, 1, (250, 250, 250))
            self.screen.blit(self.surface, self.character)
            self.screen.blit(text, self.character.center)

    def move(self):
        """Move the potato to right."""
        if self.character.left < self.stage.width:
            self.collision_area = self.collision_area.move(self.speed/self.FPS, 0)
            self.character = self.character.move(self.speed/self.FPS, 0)
        if self.character.left >= self.stage.width:
            self.has_passed = True

    def get_collision_area(self):
        return self.collision_area

    def colides_with(self, itens_list):
        return self.collision_area.collidelist(itens_list) >= 0

    def attack (self,tower):
        tower.health -= self.damage

