import pygame
import os
from Map import Map

class Enemy(object):
    def __init__(self, screen, stage, fps, speed=30):
        self.stage = stage
        self.screen = screen
        self.FPS = fps
        #speed of the enemy (pixels/second)
        self.speed = speed
        self.alive = True
        self.has_passed = False