#!/usr/bin/env python

#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/
#
# Kartoffelneinmarsch - No potato shall survive.
#
# Authors:
#    Adolfo Schneider
#    Lucas Kreutz [http://lucaskreutz.com.br]
#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/#/

import os
import sys
import pygame
import time

from pygame.locals import *
from pygame import Surface

from Map import Map
from Menu import Menu
from Potato import Potato
from Tower import Tower
from Level import Level
from Wave import Wave
from Player import Player
from SweetPotato import SweetPotato
from FrenchFries import FrenchFries

def show_potatoes(potatoes):
    """show a list of potatoes recursively"""
    if len(potatoes) == 1:
        potatoes[0].show()
        return potatoes[0]
    elif len(potatoes) > 1:
        potatoes[0].show()
        return [potatoes[0], show_potatoes(potatoes[1:])]

def main():
    """Main function of the game"""

    # intializes pygame
    print "Loading pygame..."
    pygame.init()
    pygame.display.set_caption("Kartoffelneinmarsch")
    pygame.mouse.set_visible(True)
    screen = pygame.display.set_mode((800, 600))
    clock = pygame.time.Clock()
    FPS = 30

    # divides the screen in the game area and the tool box
    fence = Rect((0,0),(800,80))
    stage = Rect((0,80), (800,420))
    menu_area = Rect((0,500), (800,100))

    fence_img = pygame.image.load(os.path.join('img', 'fence.png')).convert()
    fence_surface = pygame.Surface((800, 80))
    for y in range(0,80,fence_img.get_height()):
        for x in range(0,800,fence_img.get_width()):
            fence_surface.blit(fence_img, (x,y))

    #initiating a new game
    level_number = 1

    player_health = 100
    player_points = 100
    player = Player(player_health,player_points,level_number) 

    temp_level = Level(screen, os.path.join('levels', str(level_number) + '.json'))

    # load the map
    print "Loading map..."
    game_map = Map(screen, stage)
    game_map.show()

    # load the menu
    print "Loading menu..."
    menu = Menu(screen, menu_area, temp_level, player)
    menu.show()

    # enter in the main loop
    pygame.display.flip()
    playing = True
    temp_tower = None
    new_wave = True
    new_level = True
    restart = False
    game_over = False
    towers = list()

    print "Started main loop"
    while True:
        screen.blit(fence_surface, fence)

        if new_level or restart:
            try:
                filepath = os.path.join('levels',str(player.current_level_number) + '.json')
                current_level = Level(screen, filepath)
                menu.level = current_level
                new_level = False
            except IOError:
                font = pygame.font.Font(None, 70)
                s = "Congratulations, you won the game!"
                text = font.render(s, 1, (0, 0, 0))
                screen.blit(text, (stage.left, stage.centery))
                playing = False
                pygame.display.update()


        for event in pygame.event.get():
            if event.type == QUIT:
               pygame.quit()
               sys.exit()
            elif event.type == MOUSEBUTTONUP:
                menu.click()
            elif event.type == MOUSEMOTION:
                mouse_pos = pygame.mouse.get_pos()
                temp_tower = Tower(screen, stage)

        if playing:
            game_map.show()
            menu.show()
                        
            #load the potatoes
            if new_wave: 
                potatoes = list()
                current_wave = current_level.waves[current_level.current_wave_number-1]
                for i in range(current_wave.number_of_potatoes):
                    speed = current_level.waves[current_level.current_wave_number-1].speed
                    if current_wave.enemy_type == "Potatoes":
                        potatoes.append(Potato(screen, stage, FPS, speed, -50-i*10))
                    if current_wave.enemy_type == "Sweet Potatoes":
                        potatoes.append(SweetPotato(screen, stage, FPS, speed, -50-i*10))
                    if current_wave.enemy_type == "French Fries":
                        potatoes.append(FrenchFries(screen, stage, FPS, speed, -50-i*10))
                new_wave = False

            show_potatoes(potatoes)

            if not temp_tower is None:
                if menu.tower_is_selected():
                    temp_tower.show_temporary(mouse_pos)
                else:
                    if temp_tower.is_in_stage():
                        towers_collision_areas = [t.get_collision_area() for t in towers]
                        if not temp_tower.colides_with(towers_collision_areas) and (player.points >= temp_tower.price):
                            player.decrease_points(temp_tower.price)
                            temp_tower.fix_tower()
                            towers.append(temp_tower)
                    temp_tower = None

            for tower in towers:
                if not (tower.health > 0):
                    towers.remove(tower)

            for tower in towers:
                tower.show()

            pygame.display.update()

            #fight time
            for potato in potatoes:
                for tower in towers:
                    if tower.range_area.colliderect(potato.get_collision_area()):
                        tower.attack(potato)
                    if tower.base_area.colliderect(potato.get_collision_area()):
                        potato.attack(tower)
                        
            # high-order function used to clean the dead potatoes
            potatoes = filter(lambda p: p.health > 0, potatoes)

            #potato movements            
            towers_collision_areas = [t.get_collision_area() for t in towers]
            if potatoes: 
                for potato in potatoes:    
                    if not potato.colides_with(towers_collision_areas):
                        potato.move()
                        if (potato.has_passed and (player.health > 0)):
                            potatoes.remove(potato)
                            player.decrease_health(1)                       
                        if not potatoes and (player.health > 0):
                            new_wave = True
                            current_level.current_wave_number+=1
            else:
                new_wave = True
                player.increase_points(current_level.waves[current_level.current_wave_number-1].award)
                current_level.current_wave_number += 1
            
            #next level
            if new_wave and (current_level.current_wave_number > current_level.number_of_waves):
                new_level = True
                player.current_level_number += 1

            #game over
            if (player.health <= 0):
                font = pygame.font.Font(None, 70)
                s = "Game Over"
                text = font.render(s, 1, (0, 0, 0))
                screen.blit(text, (stage.left, stage.centery))
                game_over = True
                playing = False
                menu.show()
                pygame.display.update()


        clock.tick(FPS);


if __name__ == '__main__':
    main()